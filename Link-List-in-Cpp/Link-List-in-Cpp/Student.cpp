//
//  Student.cpp
//  Link-List-in-Cpp
//
//  Created by 买明 on 23/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include "Student.hpp"

Student::Student(int no, int age) {
    this->no = no;
    this->age = age;
}

ostream &operator<<(ostream &out, Student &s) {
    
    out << "[" << s.no << ": " << s.age << "]";
    return out;
}

bool Student::operator==(Student &s) {
    return this->no == s.no && this->age == s.age;
}
