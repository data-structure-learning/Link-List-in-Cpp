//
//  Student.hpp
//  Link-List-in-Cpp
//
//  Created by 买明 on 23/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Student_hpp
#define Student_hpp

#include <stdio.h>
#include <ostream>

using namespace std;

class Student {
    friend ostream &operator<<(ostream &out, Student &s);
    
    int no;
    int age;
    
public:
    Student(int no = 0, int age = 0);
    bool operator==(Student &s);
};

#endif /* Student_hpp */
