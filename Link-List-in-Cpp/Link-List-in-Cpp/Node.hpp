//
//  Node.hpp
//  Link-List-in-Cpp
//
//  Created by 买明 on 23/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

template <typename T>
class Node {
    
public:
    T data;
    Node *next;
    
    void printInfo();
};

template <typename T>
void Node<T>::printInfo() {
    cout << data << " ";
}

#endif /* Node_hpp */
