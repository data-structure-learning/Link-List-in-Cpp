//
//  LinkList.hpp
//  Link-List-in-Cpp
//
//  Created by 买明 on 23/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef LinkList_hpp
#define LinkList_hpp

#include <stdio.h>
#include "Node.hpp"

template <typename T>
class LinkList {

public:
    LinkList();
    ~LinkList();
    void clearList();
    bool isEmpty();
    int listLength();
    bool getElem(int i, Node<T> *pNode);
    int locateElem(Node<T> *pNode);
    bool preElem(Node<T> *pCurNode, Node<T> *pPreNode);
    bool nextElem(Node<T> *pCurNode, Node<T> *pNextNode);
    void listTraverse();
    bool insertList(int i, Node<T> *pNode);
    bool deleteList(int i, Node<T> *pNode);
    
private:
    Node<T> *m_pList;
    int m_iLength;
};

template <typename T>
LinkList<T>::LinkList() {
    m_pList = new Node<T>;
    m_pList->data = NULL;
    m_pList->next = NULL;
    m_iLength = 0;
}

template <typename T>
LinkList<T>::~LinkList() {
    clearList();
    delete m_pList;
    m_pList = NULL;
}

template <typename T>
void LinkList<T>::clearList() {
    Node<T> *temp = m_pList->next;
    while (temp != NULL) {
        Node<T> *t = temp->next;
        delete temp;
        temp = t;
    }
    m_pList->next = NULL;
}

template <typename T>
bool LinkList<T>::isEmpty() {
    return m_iLength == 0;
}

template <typename T>
int LinkList<T>::listLength() {
    return m_iLength;
}

template <typename T>
bool LinkList<T>::getElem(int i, Node<T> *pNode) {
    if (i < 0 || i >= m_iLength) {
        return false;
    }
    
    Node<T> *temp = m_pList;
    int n = 0;
    while (temp->next != NULL) {
        temp = temp->next;
        if (n == i) {
            pNode->data = temp->data;
            return true;
        }
        n += 1;
    }
    
    return false;
}

template <typename T>
int LinkList<T>::locateElem(Node<T> *pNode) {
    Node<T> *temp = m_pList;
    int n = 0;
    while (temp->next != NULL) {
        temp = temp->next;
        if (temp->data == pNode->data) {
            return n;
        }
        n += 1;
    }
    
    return -1;
}

template <typename T>
bool LinkList<T>::preElem(Node<T> *pCurNode, Node<T> *pPreNode) {
    Node<T> *curNode = m_pList;
    Node<T> *temp = m_pList;
    
    while (curNode->next != NULL) {
        temp = curNode;
        curNode = curNode->next;
        if (curNode->data == pCurNode->data) {
            if (temp == m_pList) {
                return false;
            }
            
            pPreNode->data = temp->data;
            return true;
        }
    }
    
    return false;
}

template <typename T>
bool LinkList<T>::nextElem(Node<T> *pCurNode, Node<T> *pNextNode) {
    Node<T> *temp = m_pList;
    
    while (temp->next != NULL) {
        temp = temp->next;
        if (temp->data == pCurNode->data) {
            if (temp->next == NULL) {
                return false;
            }
            pNextNode->data = temp->next->data;
            return true;
        }
    }
    
    return false;
}

template <typename T>
void LinkList<T>::listTraverse() {
    Node<T> *temp = m_pList;
    
    while (temp->next != NULL) {
        temp = temp->next;
        temp->printInfo();
    }
    
    cout << endl;
}

template <typename T>
bool LinkList<T>::insertList(int i, Node<T> *pNode) {
    if (i < 0 || i > m_iLength) {
        return false;
    }
    
    Node<T> *temp = m_pList;
    
    for (int n = 0; n < i; n++) {
        temp = temp->next;
    }
    
    Node<T> *newNode = new Node<T>;
    if (newNode == NULL) {
        return false;
    }
    
    newNode->data = pNode->data;
    newNode->next = temp->next;
    temp->next = newNode;
    
    m_iLength += 1;
    return true;
}

template <typename T>
bool LinkList<T>::deleteList(int i, Node<T> *pNode) {
    if (i < 0 || i >= m_iLength) {
        return false;
    }
    
    Node<T> *temp = m_pList;
    for (int n = 0; n <= i; n++) {
        pNode = temp;
        temp = temp->next;
    }
    
    pNode->next = temp->next;
    delete temp;
    temp->next = NULL;
    return true;
}

#endif /* LinkList_hpp */
