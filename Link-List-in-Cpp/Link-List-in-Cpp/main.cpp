//
//  main.cpp
//  Link-List-in-Cpp
//
//  Created by 买明 on 23/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <iostream>
#include "LinkList.hpp"
#include "Student.hpp"

using namespace std;

void testIntList();
void testObjectList();

int main(int argc, const char * argv[]) {
    cout << "testIntList" << endl;
    testIntList();
    
    cout << "testObjectList" << endl;
    testObjectList();
    return 0;
}

void testIntList() {
    LinkList<int> *linkList = new LinkList<int>();
    
    Node<int> *pNode_1 = new Node<int>();
    Node<int> *pNode_2 = new Node<int>();
    Node<int> *pNode_3 = new Node<int>();
    Node<int> *pNode_4 = new Node<int>();
    Node<int> *pNode_5 = new Node<int>();
    Node<int> *pNode_6 = new Node<int>();
    Node<int> *pNode_7 = new Node<int>();
    Node<int> *pNode_8 = new Node<int>();
    Node<int> *pNode_9 = new Node<int>();
    Node<int> *pNode_10 = new Node<int>();
    
    pNode_1->data = 1;
    linkList->insertList(0, pNode_1);
    pNode_2->data = 2;
    linkList->insertList(1, pNode_2);
    pNode_3->data = 3;
    linkList->insertList(2, pNode_3);
    pNode_4->data = 4;
    linkList->insertList(3, pNode_4);
    pNode_5->data = 5;
    linkList->insertList(4, pNode_5);
    
    linkList->listTraverse();
    
    pNode_10->data = 10;
    linkList->insertList(1, pNode_10);
    
    linkList->listTraverse();
    
    pNode_6->data = 10;
    cout << linkList->locateElem(pNode_6) << endl;
    
    linkList->getElem(1, pNode_7);
    cout << pNode_7->data << endl;
    
    linkList->nextElem(pNode_5, pNode_8);
    cout << pNode_8->data << endl;
    
    linkList->preElem(pNode_10, pNode_8);
    cout << pNode_8->data << endl;
    
    linkList->listTraverse();
    
    linkList->deleteList(5, pNode_9);
    
    linkList->listTraverse();
    
    linkList->clearList();
    
    linkList->listTraverse();
}

void testObjectList() {
    LinkList<Student> *linkList = new LinkList<Student>();
    
    
    Node<Student> *pNode_1 = new Node<Student>();
    
    Node<Student> *pNode_2 = new Node<Student>();
    Node<Student> *pNode_3 = new Node<Student>();
    Node<Student> *pNode_4 = new Node<Student>();
    Node<Student> *pNode_5 = new Node<Student>();
    Node<Student> *pNode_6 = new Node<Student>();
    Node<Student> *pNode_7 = new Node<Student>();
    Node<Student> *pNode_8 = new Node<Student>();
    Node<Student> *pNode_9 = new Node<Student>();
    Node<Student> *pNode_10 = new Node<Student>();
    
    Student *stu = new Student(1, 1);
    pNode_1->data = *stu;
    linkList->insertList(0, pNode_1);
    
    stu = new Student(2, 2);
    pNode_2->data = *stu;
    linkList->insertList(1, pNode_2);
    
    stu = new Student(3, 3);
    pNode_3->data = *stu;
    linkList->insertList(2, pNode_3);
    
    stu = new Student(4, 4);
    pNode_4->data = *stu;
    linkList->insertList(3, pNode_4);
    
    stu = new Student(5, 5);
    pNode_5->data = *stu;
    linkList->insertList(4, pNode_5);
    
    linkList->listTraverse();
    
    stu = new Student(10, 10);
    pNode_10->data = *stu;
    linkList->insertList(1, pNode_10);
    
    linkList->listTraverse();
    
    pNode_6->data = *stu;
    cout << linkList->locateElem(pNode_6) << endl;
    
    linkList->getElem(1, pNode_7);
    cout << pNode_7->data << endl;
    
    linkList->nextElem(pNode_5, pNode_8);
    cout << pNode_8->data << endl;
    
    linkList->preElem(pNode_10, pNode_8);
    cout << pNode_8->data << endl;
    
    linkList->listTraverse();
    
    linkList->deleteList(5, pNode_9);
    
    linkList->listTraverse();
    
    linkList->clearList();
    
    linkList->listTraverse();
}

